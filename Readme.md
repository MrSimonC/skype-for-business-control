# Skype for Business Status Control

Control your status on Skype for Business 2016 using this PowerShell Script.

Example use: lock your PC and put Skype into Do Not Disturb, which prevents calls when the PC is locked.

## Usage

Unzip all files to a directory (e.g. `C:\Program Files\Skype For Business Control\`.

Adjust windows policy for PowerShell to "RemoteSigned" & unlock the files so that PowerShell will run the scripts:

```powershell
Set-ExecutionPolicy RemoteSigned
dir "C:\Program Files\Skype For Business Control\" | Unblock-File
```

Run with command line:

```batch
powershell ./change-status.ps1 -status <STATUS>
```

Where `<STATUS>` is "Available", "Away", "Busy", "DoNotDisturb", "BeRightBack"

Example:

```batch
change-status.ps1 -status Away
```

Either use with a shortcut using the AutoHotKeyFile [shortcuts.ahk](shortcuts.ahk), or with Windows Task Scheduler (which can bind to workstation Lock / Unlock directly).

### Windows Task Scheduler

Use "Task", "Import" and choose the appropriate .xml files (note, you must change the "logged in user" to yourself), or manually create a new task with the following settings:

* Trigger: On workstation lock
* Action:
  * Program/Script: `Powershell.exe`
  * Add arguments: `-WindowStyle Hidden -File "C:\Program Files\Skype For Business Control\change-status.ps1" -status DoNotDisturb`

and

* Trigger: On workstation unlock
* Action:
  * Program/Script: `Powershell.exe`
  * Add arguments: `-WindowStyle Hidden -File "C:\Program Files\Skype For Business Control\change-status.ps1" -status Available`

## Background

It was created by a lot of research! Using old [Lync 2013 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=36824) which I downloaded, then unzipped the .exe file using 7zip, then install the .msi, find the dlls, work out which are called and include in this project.