; Sample autohotkey file to control status directly
; Available status': "Available", "Away", "Busy", "DoNotDisturb", "BeRightBack"

; Available = alt+shift+a
!+a::
    setStatus("Available")
return

; Busy = alt+shift+b
!+b::
    setStatus("Busy")
return

; DoNotDisturb = alt+shift+d (for "Windows Lock" instead of #l as a shortbut, consider using windows task manager for lock / unlock directly)
!+d::
    setStatus("DoNotDisturb")
return

SetStatus(status)
{
    SetWorkingDir %A_ScriptDir%     ;look for the powershell script in the same directory as this script
    Run, powershell -WindowStyle Hidden -ExecutionPolicy ByPass -File "change-status.ps1" %status%
}