# Might need admin: Set-ExecutionPolicy RemoteSigned
# Change the setting in skype to a value passed in via argument to this script
# e.g. change-status.ps1 -status DoNotDisturb

Param(
    [Parameter(Mandatory = $true, Position = 1)]
    [ValidateNotNullOrEmpty()]
    [ValidateSet("Available", "Away", "Busy", "DoNotDisturb", "BeRightBack")]
    [string]$status
)

enum Status {
    Available = 3500
    Away = 15500
    Busy = 6500
    DoNotDisturb = 9500
    BeRightBack = 12000
}

function SetStatus {
    Param(
        [Parameter(Mandatory = $true, Position = 1)]
        [ValidateNotNullOrEmpty()]
        [Status]$status
    )    

    # import dlls
    function Get-ScriptDirectory {
        Split-Path $script:MyInvocation.MyCommand.Path
    }
    $ControlsPath = Get-ScriptDirectory | Join-Path -ChildPath "Microsoft.Lync.Controls.dll"
    $ModelPath = Get-ScriptDirectory | Join-Path -ChildPath "Microsoft.Lync.Model.dll"
    import-module $ControlsPath
    import-module $ModelPath

    # Obtain the entry point to the Lync.Model API
    $client = [Microsoft.Lync.Model.LyncClient]::GetClient()
    $self = $client.Self;

    # Set Details of Personal Note and Availability
    # Useful availability codes for use below - 3500 Available, 15500 Away (converted to "Off Work" in this script by setting activity ID), 6500 Busy, 9500 Do not disturb, 12000 Be Right Back)
    $availability = [Int32]$status
    Write-Output $availability
    # $date = [DateTime]::Now
    # $message = "Remote Work. UK Time +5 hours EST (US), -5.5 IST (India). Availability script last run $date"

    # Publish personal note and presence availability of the local user
    $contactInfo = new-object 'System.Collections.Generic.Dictionary[Microsoft.Lync.Model.PublishableContactInformationType, object]'
    # $contactInfo.Add([Microsoft.Lync.Model.PublishableContactInformationType]::PersonalNote, $message)
    $contactInfo.Add([Microsoft.Lync.Model.PublishableContactInformationType]::Availability, $availability)
    # If ($availability -eq 15500) {$contactInfo.Add([Microsoft.Lync.Model.PublishableContactInformationType]::ActivityId, "off-work")}

    $ar = $self.BeginPublishContactInformation($contactInfo, $null, $null)
    $self.EndPublishContactInformation($ar)
}

SetStatus $status